\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\bibliography{references.bib}

\DeclarePairedDelimiterX{\set}[2]{\{}{\}}{#1 \;\delimsize\vert\; #2}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\curlybr}{\{}{\}}
\DeclarePairedDelimiter{\roundbr}{(}{)}

\newcommand*{\RR}{\mathbf{R}}
\newcommand*{\ZZ}{\mathbf{Z}}

\newcommand*{\calG}{\mathcal{G}}
\newcommand*{\calO}{\mathcal{O}}
\newcommand*{\calT}{\mathcal{T}}

\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\intAut}{\underline{Aut}}

\newcommand*{\catmods}{\mathsf{Mod}}
\newcommand*{\catreps}{\mathsf{Rep}}
\newcommand*{\catnormed}{\mathsf{Norm}}


\title{Kleine AG: Normed fiber functors}
\author{Program: Manuel Hoff}
\date{Summer term 2024}

\begin{document}
    
    \maketitle

    \section*{Introduction}

    Fix a complete non-archimedean discretely valued field $K$ with (additive) valuation $\nu \colon K \to \RR \cup \curlybr{\infty}$ and a split connected reductive group $G$ over $K$.
    Also fix a reductive model $\calG$ of $G$ over $\calO_K$.

    By work of Bruhat and Tits we then have the following objects.

    \begin{itemize}
        \item
        The \emph{extended Bruhat-Tits building} $I(G)$.
        This is a (typically infinite) polysimplicial complex that comes equipped with an action of the group $G(K)$.
        The chosen reductive model $\calG$ gives rise to a base point $x_{\calG} \in I(G)$ with stabilizer $\calG(\calO_K)$.
        
        \item
        The \emph{Bruhat-Tits group scheme} $\calG_x$ (for a point $x \in I(G)$).
        This is a smooth affine group scheme over $\calO_K$ with generic fiber $G$ and such that $\calG_x(\calO_K) \subseteq G(K)$ is precisely the stabilizer of $x \in I(G)$.
    \end{itemize}

    \begin{example}
        Let $V$ be a finite dimensional $K$-vector space.
        Then the extended Bruhat-Tits building can be described as
        \[
            I \roundbr[\big]{\GL(V)} \cong \curlybr[\Big]{\text{(additive) norms $\alpha \colon V \to \RR \cup \curlybr{\infty}$}}.
        \]
        Moreover, for $x = \alpha \in I(\GL(V))$ we have an associated $\calO_K$-lattice chain $(V^{\alpha \geq t})_{t \in \RR}$ and $\calG_x$ is the automorphism group scheme of this lattice chain.
    \end{example}

    The goal of this Kleine AG is to take a look at Paul Ziegler's article \cite{ziegler} that gives a Tannakian generalization of the above picture for general $G$, extending work of Wilson and Cornut.
    The main result of the article is the following.

    \begin{theorem} \label{thm:main}
        We have a natural $G(K)$-equivariant bijection
        \[
            I(G) \cong N^{\otimes}(\calG) \coloneqq \curlybr[\Big]{\text{norms $\alpha$ on the standard fiber functor $\omega_{\calG} \colon \catreps_{\calO_K}(\calG) \to \catmods_K$}}.
        \]
        Moreover, for $x = \alpha \in I(G)$ we have a natural isomorphism $\calG_x \cong \intAut^{\otimes}(\alpha)$.
    \end{theorem}



    The first three talks will focus on understanding the relevant definitions in \cite{ziegler}.
    In particular we will learn what the objects $N^{\otimes}(\calG)$ and $\intAut^{\otimes}(\alpha)$ are.
    In the last talk will then sketch the proof of \Cref{thm:main}.

    \section*{Talks}

    Each talk should be around 60-70 minutes.
    
    \begin{enumerate}[label = \textbf{Talk \arabic*:}]
        \setcounter{enumi}{0} 
        
        \item
        \textbf{Representations of Algebraic groups (by Sarah).}

        Define the notions of \emph{$R$-linear exact rigid tensor categories} and \emph{$R$-linear exact tensor functors} (for some ring $R$).
        Give the example of the category $\catmods_R$ of finite projective $R$-modules.

        Given an algebraic group $H$ over $R$ (i.e.\ a flat affine group scheme of finite presentation), define the category $\catreps_R(H)$ of \emph{representations of $H$ in finite projective $R$-modules} and explain that it is an $R$-linear exact rigid tensor category.
        Explicitly describe $\catreps_R(H)$ in terms of graded modules when $H$ is a split torus.

        Define the \emph{standard fiber functor} $\omega_H \colon \catreps_R(H) \to \catmods_R$ and state the result that if $R$ is a field or a Dedekind ring (e.g.\ $K$ or $\calO_K$) then $H$ can be recovered as the automorphism group scheme $\intAut^{\otimes}(\omega_H)$ of $\omega_H$ (see \cite{broshi}).

        \item
        \textbf{Normed vector spaces and the Tannakian building (by Thiago).}

        Define what a \emph{norm} on a finite dimensional $K$-vector space is (\cite[Definition 2.1]{ziegler}).
        Classify norms in terms of lattice chains and in particular show that every norm is splittable.
        Note that we are working in the special situation where $\Gamma = \RR$ and the valuation on $K$ is discrete.

        Prove that the category $\catnormed_K$ of normed $K$-vector spaces is an $\calO_K$-linear exact rigid tensor category (\cite[Section 2.1]{ziegler}).

        Define the notion of a \emph{norm} on a fiber functor $\omega \colon \catreps_{\calO_K}(\calG) \to \catmods_K$ (\cite[Definition 4.1, Definition 3.23]{ziegler}) and the Tannakian building $N^{\otimes}(\calG)$ with its $G(K)$-action (\cite[Definition 4.3]{ziegler}).

        Show that when $G = T$ is a split torus (in this case $\calG = \calT$ is uniquely determined) we have an explicit natural bijection $N^{\otimes}(\calT) \cong \RR \otimes_{\ZZ} X_*(T)$ (\cite[Proposition 4.13]{ziegler}).

        \item
        \textbf{Stabilizer groups (by Sriram).}

        Given a normed $K$-vector space $(V, \alpha)$ define its stabilizer group $\intAut(\alpha)$ (\cite[Definition 2.20]{ziegler}).
        This is an affine group scheme of finite type over $\calO_K$ with generic fiber $\GL(V)$.
        Note that $\intAut(\alpha)$ is smooth by a result of Rapoport and Zink.

        More generally also define the stabilizer group scheme $\intAut^{\otimes}(\alpha)$ of a norm $\alpha$ on a fiber functor $\omega \colon \catreps_{\calO_K}(\calG) \to \catmods_K$ (\cite[Definition 3.28]{ziegler}) and sketch the proof of the smoothness of $\intAut^{\otimes}(\alpha)$ (\cite[Theorem 3.66]{ziegler}).


        \item
        \textbf{Comparison (by Giulio).}

        Sketch the proof of the main theorem \cite[Theorem 4.16]{ziegler}, see also \cite{cornut}.
    \end{enumerate}

    \printbibliography
\end{document}
