# ss2024-kleine-ag-normed-fiber-functors

This is the program for the Kleine AG on "Normed fiber functors" that will take place in Essen on the 13th of April in 2024.
A compiled version of the program can be found [here](https://makoba.gitlab.io/ss2024-kleine-ag-normed-fiber-functors/normed-fiber-functors-program.pdf).